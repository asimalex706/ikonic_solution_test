<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserConnectionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::get('/get_suggestions', [UserConnectionController::class, 'showSuggestions'])->name('get_suggestions');
    Route::post('connect', [UserConnectionController::class, 'sendConnectionRequest'])->name('connect');
    Route::get('/get_sent_requests', [UserConnectionController::class, 'showSentRequests'])->name('get_sent_requests');
    Route::delete('withdraw_connection_request/{user}', [UserConnectionController::class, 'withdrawConnectionRequest'])->name('withdraw_connection_request');
    Route::get('get_received_requests', [UserConnectionController::class, 'showReceivedRequests'])->name('get_received_requests');
    Route::put('accept_connection_request/{user}', [UserConnectionController::class, 'acceptConnectionRequest'])->name('accept_connection_request');
    Route::get('get_connections', [UserConnectionController::class, 'showConnections'])->name('get_connections');
    Route::delete('remove_connection/{user}', [UserConnectionController::class, 'removeConnection'])->name('remove_connection');
    Route::get('connections-in-common', [UserConnectionController::class, 'showConnectionsInCommon'])->name('connections-in-common');
