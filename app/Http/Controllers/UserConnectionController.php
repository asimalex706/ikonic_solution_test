<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Connection;

class UserConnectionController extends Controller
{
    public function showSuggestions()
    {
        $currentUser = auth()->user();
        $suggestions = User::whereNotIn('id', $currentUser->connections->pluck('id'))
                           ->whereNotIn('id', $currentUser->sentRequests->pluck('connected_user_id'))
                           ->whereNotIn('id', $currentUser->receivedRequests->pluck('user_id'))
                           ->where('id', '!=', $currentUser->id)
                           ->get();
    
        return view('components/suggestion', compact('suggestions'));
    }
    

    public function sendConnectionRequest(Request $request)
    {
        $currentUser = auth()->user();
        $user=$request->user_id;

        if ($user === $currentUser->id) {
            return response()->json(['message' => 'You cannot send a connection request to yourself.'], 400);
        }

        if ($currentUser->connections->contains($user)) {
            return response()->json(['message' => 'You are already connected to this user.'], 400);
        }

        if ($currentUser->sentRequests->contains('connected_user_id', $user)) {
            return response()->json(['message' => 'Connection request already sent.'], 400);
        }

        if ($currentUser->receivedRequests->contains('user_id', $user)) {
            return response()->json(['message' => 'You have already received a connection request from this user.'], 400);
        }
        $connection = new Connection([
            'user_id' => $currentUser->id,
            'connected_user_id' => $user,
            'status' => 'pending'
        ]);
        $connection->save();

        return response()->json(['message' => 'Connection request sent successfully.'], 200);
    }

    public function showSentRequests()
    {
        $currentUser = auth()->user();
        $sentRequests = $currentUser->sentRequests;
        return view('components/request', compact('sentRequests'));
    }

    public function withdrawConnectionRequest(User $user)
    {
        $currentUser = auth()->user();

        $connection = $currentUser->sentRequests->where('connected_user_id', $user->id)->first();

        if (!$connection) {
            return response()->json(['message' => 'Connection request not found.'], 404);
        }

        $connection->delete();

        return response()->json(['message' => 'Connection request withdrawn successfully.'], 200);
    }

    public function showReceivedRequests()
    {
        $currentUser = auth()->user();
        $receivedRequests = $currentUser->receivedRequests;
        return view('components/request', compact('receivedRequests'));
    }

    public function acceptConnectionRequest(User $user)
    {
        $currentUser = auth()->user();
        $connection = $currentUser->receivedRequests->where('user_id', $user->id)->first();

        if (!$connection) {
            return response()->json(['message' => 'Connection request not found.'], 404);
        }

        $connection->status = 'accepted';
        $connection->save();

        return response()->json(['message' => 'Connection request accepted successfully.'], 200);
    }

    public function showConnections()
    {
        $currentUser = auth()->user();
        $connections = $currentUser->connections;
        return view('components/connection', compact('connections'));
    }

    public function removeConnection(User $user)
    {
        dd(1);
        $currentUser = auth()->user();

        $connection = $currentUser->connections->where('connected_user_id', $user->id)->first();

        if (!$connection) {
            return response()->json(['message' => 'Connection not found.'], 404);
        }

        $connection->delete();

        return response()->json(['message' => 'Connection removed successfully.'], 200);
    }

    public function showConnectionsInCommon(User $user)
    {
        $currentUser = auth()->user();
        $connectionsInCommon = $currentUser->connectionsInCommon($user)->get();

        return view('components/connection_in_common', compact('connectionsInCommon'));
    }

}
