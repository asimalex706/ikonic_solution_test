<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as AuthenticatableUser;
use Illuminate\Notifications\Notifiable;

class User extends AuthenticatableUser implements Authenticatable
{
    protected $fillable = ['name', 'email', 'password'];


    public function sentRequests()
    {
        return $this->hasMany(Connection::class, 'user_id', 'id')
                    ->where('status', 'pending')
                    ->with('connectedUser');
    }
    public function receivedRequests()
    {
        return $this->hasMany(Connection::class, 'connected_user_id', 'id')
                    ->where('status', 'pending')
                    ->with('RequestUser');
    }

    public function connections()
    {
        return $this->hasMany(Connection::class, 'connected_user_id', 'id')
                    ->where('status', 'accepted')
                    ->with('ConnectionUser');
    }

    public function connectionsInCommon(User $user)
    {
        return $this->connections()->whereIn('connected_user_id', $user->connections->pluck('user_id'));
    }
}
