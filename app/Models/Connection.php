<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Connection extends Model
{
    use HasFactory;
    protected $table = 'user_connections';
    // Define the fillable columns
    protected $fillable = [
        'user_id',
        'connected_user_id',
        'status'
    ];
    public function connectedUser()
    {
        return $this->belongsTo(User::class, 'connected_user_id', 'id');
    }
    public function RequestUser()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function ConnectionUser()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
