@foreach ($connections as $connection)

<div class="my-2 shadow text-white bg-dark p-1" id="">
  <div class="d-flex justify-content-between">
    <table class="ms-1">
            <tr>
                <td class="align-middle">Name</td>
                <td class="align-middle">{{ $connection->ConnectionUser->name}}</td>
                <td class="align-middle">Email</td>
                <td class="align-middle">{{$connection->ConnectionUser->email}}</td>
                <button class="btn btn-danger me-1" onclick="removeConnection({{ $connection->ConnectionUser->id }})">Remove Connection </button>
            </tr>
    </table>
    <div>
  </div>
  @endforeach

  <div class="collapse" id="collapse_">

    <div id="content_" class="p-2">
      {{-- Display data here --}}
    </div>
    <div id="connections_in_common_skeletons_">
        <x-connection_in_common />
    </div>
    <div class="d-flex justify-content-center w-100 py-2">
      <button class="btn btn-sm btn-primary" id="load_more_connections_in_common_">Load
        more</button>
    </div>
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
  // public/js/main.js

// Fetch connections in common and update the UI
function getConnectionsInCommon() {
  // Show loading skeletons before the AJAX request
  $("#content_").hide();
  $("#connections_in_common_skeletons_").show();

  // Make the AJAX request
  $.ajax({
    url: "connections-in-common", // Replace {user_id} with the actual user ID you want to get connections in common with
    method: "GET",
    success: function (response) {
      // Update the UI with the received data
      $("#connections_in_common_skeletons_").hide();
      $("#content_").html(response);
      $("#content_").show();
    },
    error: function (error) {
      console.log(error);
    },
  });
}

// Call getConnectionsInCommon when "Connections in common" button is clicked
$("#get_connections_in_common_").on("click", function () {
  getConnectionsInCommon();
});

// public/js/main.js

// Remove a connection and update the UI
function removeConnection(userId) {
    $.ajax({
      url: '/remove_connection/' + userId,
      type: 'DELETE',
      data: {
        _token: '{{ csrf_token() }}',
      },
      beforeSend: function () {
        // Show loading state or disable the button if needed
      },
      success: function (response) {
        // Update the UI to reflect the request withdrawal
        alert('Connection Deleted successfully.');
      },
      error: function () {
        // Handle error, if any
        alert('Failed to Delete connection.');
      },
    });
  }

// Call removeConnection when "Remove Connection" button is clicked
$(".remove-connection-btn").on("click", function () {
    var userId = $(this).data('user-id');
    removeConnection(userId);
  });

// public/js/main.js

// Load more connections in common and update the UI
function loadMoreConnectionsInCommon() {
  // Show loading skeletons before the AJAX request
  $("#load_more_connections_in_common_").hide();
  $("#connections_in_common_skeletons_").show();

  // Make the AJAX request
  $.ajax({
    url: "/user/connections/connections-in-common/{user_id}", // Replace {user_id} with the actual user ID you want to get connections in common with
    method: "GET",
    data: {
      // You can pass additional data for pagination, e.g., offset or page number
      // For example: offset: 10, page: 2
    },
    success: function (response) {
      // Update the UI with the received data
      $("#connections_in_common_skeletons_").hide();
      $("#content_").append(response);
      $("#content_").show();
      $("#load_more_connections_in_common_").show(); // Show the "Load more" button again if there are more connections
    },
    error: function (error) {
      console.log(error);
    },
  });
}

// Call loadMoreConnectionsInCommon when "Load more" button is clicked
$("#load_more_connections_in_common_").on("click", function () {
  loadMoreConnectionsInCommon();
});

</script>