@if(!empty($sentRequests))
@foreach ($sentRequests as $request)

<div class="my-2 shadow text-white bg-dark p-1" id="">
    <div class="d-flex justify-content-between">
        <table class="ms-1">
            <tr>
                <td class="align-middle">Name</td>
                <td class="align-middle">{{ $request->connectedUser->name}}</td>
                <td class="align-middle">Email</td>
                <td class="align-middle">{{ $request->connectedUser->email }}</td>
                @if ($request->status == 'pending')
                  <button id="cancel_request_btn_" class="btn btn-danger me-1" onclick="withdrawConnectionRequest({{ $request->connectedUser->id }})">
                    Withdraw Request
                  </button>
                @endif
            </tr>
        </table>
    </div>
</div>
@endforeach

@else
@foreach ($receivedRequests as $request)
<div class="my-2 shadow text-white bg-dark p-1" id="">
    <div class="d-flex justify-content-between">
        <table class="ms-1">
            <tr>
                <td class="align-middle">Name</td>
                <td class="align-middle">{{ $request->RequestUser->name}}</td>
                <td class="align-middle">Email</td>
                <td class="align-middle">{{ $request->RequestUser->email }}</td>
                @if ($request->status == 'pending')
                <button id="accept_request_btn_" class="btn btn-primary me-1" onclick="acceptConnectionRequest({{ $request->RequestUser->id }})">
                    Accept
                  </button>
                @endif
            </tr>
          
        </table>
    </div>
</div>
@endforeach
 @endif

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
  // Function to handle withdrawing a connection request
  function withdrawConnectionRequest(userId) {
    $.ajax({
      url: '/withdraw_connection_request/' + userId,
      type: 'DELETE',
      data: {
        _token: '{{ csrf_token() }}',
      },
      beforeSend: function () {
        // Show loading state or disable the button if needed
      },
      success: function (response) {
        // Update the UI to reflect the request withdrawal
        alert('Connection request withdrawn successfully.');
      },
      error: function () {
        // Handle error, if any
        alert('Failed to withdraw connection request.');
      },
    });
  }

  // Function to handle accepting a connection request
  function acceptConnectionRequest(userId) {
    $.ajax({
      url: '/accept_connection_request/' + userId,
      type: 'PUT',
      data: {
        _token: '{{ csrf_token() }}',
      },
      beforeSend: function () {
        // Show loading state or disable the button if needed
      },
      success: function (response) {
        // Update the UI to reflect the request acceptance
        alert('Connection request accepted successfully.');
      },
      error: function () {
        // Handle error, if any
        alert('Failed to accept connection request.');
      },
    });
  }
</script>
