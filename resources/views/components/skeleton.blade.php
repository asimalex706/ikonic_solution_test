<div class="d-flex align-items-center mb-2 text-white bg-dark p-1 shadow" style="height: 45px">
  <strong class="ms-1 text-primary" id="skeleton_loading_text">Loading...</strong>
  <div class="spinner-border ms-auto text-primary me-4" role="status" aria-hidden="true"></div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>


<script>
  // Function to load the skeleton data using AJAX
  function loadSkeletonData(route, data) {
    $.ajax({
      url: route,
      type: 'GET',
      data: data,
      beforeSend: function () {
        // Show the skeleton loading component
        $('#skeleton_loading_text').text('Loading...');
        $('#skeleton_loading_text').removeClass('text-success');
        $('#skeleton_loading_text').addClass('text-primary');
        $('#skeleton').removeClass('d-none');
      },
      success: function (response) {
        // Hide the skeleton loading component after receiving the data
        $('#skeleton_loading_text').addClass('text-success');
        $('#skeleton_loading_text').removeClass('text-primary');
        $('#skeleton').addClass('d-none');

        // Update the content section with the received data
        $('#content').html(response);
        $('#content').removeClass('d-none');
      },
      error: function () {
        // Handle error, if any
        alert('Failed to load data.');
      },
    });
  }
</script>
<script>
  // ... (previously defined JavaScript code)

  // Event listener for radio buttons
  $('input[name="btnradio"]').on('change', function () {
    const selectedOption = $(this).attr('id');
    let route = '';
    let data = {};

    switch (selectedOption) {
      case 'btnradio1': // Suggestions
        route = '/get_suggestions';
        break;
      case 'btnradio2': // Sent Requests
        route = '/get_sent_requests';
        break;
      case 'btnradio3': // Received Requests
        route = '/get_received_requests';
        break;
      case 'btnradio4': // Connections
        route = '/get_connections';
        break;
      default:
        return;
    }

    // Call the loadSkeletonData function to show the skeleton loading component
    loadSkeletonData(route, data);
  });

  // ... (other JavaScript code)
</script>
