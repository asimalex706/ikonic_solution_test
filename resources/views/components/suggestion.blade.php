@foreach ($suggestions as $suggestion)
<div class="my-2 shadow text-white bg-dark p-1" id="suggestions-container">
    <div class="d-flex justify-content-between">
        <table class="ms-1">
            <tr>
                <td class="align-middle">Name</td>
                <td class="align-middle">{{ $suggestion->name }}</td>
                <td class="align-middle">Email</td>
                <td class="align-middle">{{ $suggestion->email }}</td>
                <button class="btn btn-primary create-request-btn" data-user-id="{{ $suggestion->id }}">Connect</button>
            </tr>
        </table>
    </div>
</div>
@endforeach

<!-- Include jQuery (you can use a CDN or host it locally) -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    // AJAX call to send a connection request
    $(document).ready(function () {
        $('.create-request-btn').click(function () {
            var userId = $(this).data('user-id');

            // Perform the AJAX request
            $.ajax({
                type: 'POST',
                url: '/connect', // Replace this with the actual route to your controller action
                data: {
                    user_id: userId,
                    _token: '{{ csrf_token() }}', // Add CSRF token for security
                },
                success: function (data) {
                    // Handle the response from the server if needed
                    // For example, you could show a success message or update the UI.
                    // You can access the response data using "data" variable.
                },
                error: function (xhr, status, error) {
                    // Handle errors if any
                    console.log(xhr.responseText);
                },
            });
        });
    });
</script>
