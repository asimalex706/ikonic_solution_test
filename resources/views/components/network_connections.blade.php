<div class="row justify-content-center mt-5">
  <div class="col-12">
    <div class="card shadow text-white bg-dark">
      <div class="card-header">Coding Challenge - Network connections</div>
      <div class="card-body">
        <div class="btn-group w-100 mb-3" role="group" aria-label="Basic radio toggle button group">
          <input type="radio" class="btn-check" name="btnradio" id="btnradio1" autocomplete="off" checked>
          <label class="btn btn-outline-primary" for="btnradio1" id="get_suggestions_btn">Suggestions ()</label>

          <input type="radio" class="btn-check" name="btnradio" id="btnradio2" autocomplete="off">
          <label class="btn btn-outline-primary" for="btnradio2" id="get_sent_requests_btn">Sent Requests ()</label>

          <input type="radio" class="btn-check" name="btnradio" id="btnradio3" autocomplete="off">
          <label class="btn btn-outline-primary" for="btnradio3" id="get_received_requests_btn">Received Requests()</label>

          <input type="radio" class="btn-check" name="btnradio" id="btnradio4" autocomplete="off">
          <label class="btn btn-outline-primary" for="btnradio4" id="get_connections_btn">Connections ()</label>
        </div>
        <hr>
        <div id="content" class="d-none">
          {{-- Display data here --}}
        </div>

        {{-- Skeletons for loading data --}}
        <div id="skeleton" class="d-none">
          @for ($i = 0; $i < 10; $i++)
            <x-skeleton />
          @endfor
        </div>

        {{-- "Load more"-Button --}}
        <div class="d-flex justify-content-center mt-2 py-3" id="load_more_btn_parent">
          <button class="btn btn-primary" onclick="" id="load_more_btn">Load more</button>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

  <script>
  // Function to load data using AJAX
  function loadData(route, data) {
    $.ajax({
      url: route,
      type: 'GET',
      data: data,
      beforeSend: function () {
        // Show loading skeletons while the data is being fetched
        $('#content').addClass('d-none');
        $('#skeleton').removeClass('d-none');
      },
      success: function (response) {
        // Update the content section with the received data
        $('#skeleton').addClass('d-none');
        $('#content').html(response);
        $('#content').removeClass('d-none');
      },
      error: function () {
        alert(response);
      },
    });
  }

  // Event listener for radio buttons
  $('input[name="btnradio"]').on('change', function () {
    const selectedOption = $(this).attr('id');
    let route = '';
    let data = {};

    switch (selectedOption) {
      case 'btnradio1': // Suggestions
        route = '/get_suggestions';
        break;
      case 'btnradio2': // Sent Requests
        route = '/get_sent_requests';
        break;
      case 'btnradio3': // Received Requests
        route = '/get_received_requests';
        break;
      case 'btnradio4': // Connections
        route = '/get_connections';
        break;
      default:
        return;
    }

    // Call the loadData function to fetch and display data
    loadData(route, data);
  });

  // Load data for 'Suggestions' by default on page load
  $(document).ready(function () {
    loadData('/get_suggestions', {});
  });
 

  // Add click event for "Load more" button
  $('#load_more_btn').on('click', function () {
    const currentOption = $('input[name="btnradio"]:checked').attr('id');
    let route = '';
    let data = {};

    switch (currentOption) {
      case 'btnradio1': // Suggestions
        route = '/get_more_suggestions';
        case 'btnradio2': // Suggestions
        route = '/get_sent_requests';
        // Add necessary data if needed
        break;
      // Add more cases for other options if required

      default:
        return;
    }

    // Call the loadData function to fetch and append more data
    loadData(route, data);
  });
</script>
